package com.tomcat.ocr.idcard;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.msd.ocr.idcard.LibraryInitOCR;
import com.tomcat.custom.CustomCardActivity;
import com.tomcat.custom.util.FileUtil;
import com.tomcat.ocr.idcard.databinding.ActivityMainBinding;

import org.json.JSONException;
import org.json.JSONObject;


public class MainActivity extends AppCompatActivity {
    private Context context;
    private ActivityMainBinding binding;
    public static final String KEY_OUTPUT_FILE_PATH = "outputFilePath";
    public static final String KEY_CONTENT_TYPE = "contentType";
    public static final String CONTENT_TYPE_ID_CARD_FRONT = "IDCardFront";
    public static final String CONTENT_TYPE_ID_CARD_BACK = "IDCardBack";
    private final int REQUEST_CODE = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        context = this;
        LibraryInitOCR.initOCR(context);
        initView();

    }

    private void initView() {
        binding.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customOpenNextPage();
            }
        });
        binding.button.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                binding.tip.setVisibility(View.GONE);
                return true;
            }
        });
    }

    private void customOpenNextPage() {
        Intent intent = new Intent(MainActivity.this, CustomCardActivity.class);
        intent.putExtra(MainActivity.KEY_OUTPUT_FILE_PATH,
                FileUtil.getSaveFile(getApplication()).getAbsolutePath());
        intent.putExtra(MainActivity.KEY_CONTENT_TYPE, choseType(binding.type.getSelectedItemPosition()));
        startActivityForResult(intent, REQUEST_CODE);
    }

    private String choseType(int selectedItemPosition) {
        if (selectedItemPosition == 0)
            return CONTENT_TYPE_ID_CARD_FRONT;
        else
            return CONTENT_TYPE_ID_CARD_BACK;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e("扫描-requestCode ", requestCode + "");
        Log.e("扫描-resultCode", resultCode + "");
        if (requestCode == REQUEST_CODE && resultCode == RESULT_OK) {
            String result = data.getStringExtra("OCRResult");
            try {
                JSONObject jo = new JSONObject(result);
                StringBuffer sb = new StringBuffer();
                sb.append(String.format("正面 = %s\n", jo.opt("type")));
                sb.append(String.format("姓名 = %s\n", jo.opt("name")));
                sb.append(String.format("性别 = %s\n", jo.opt("sex")));
                sb.append(String.format("民族 = %s\n", jo.opt("folk")));
                sb.append(String.format("日期 = %s\n", jo.opt("birt")));
                sb.append(String.format("号码 = %s\n", jo.opt("num")));
                sb.append(String.format("住址 = %s\n", jo.opt("addr")));
                sb.append(String.format("签发机关 = %s\n", jo.opt("issue")));
                sb.append(String.format("有效期限 = %s\n", jo.opt("valid")));
                sb.append(String.format("整体照片 = %s\n", jo.opt("imgPath")));
                sb.append(String.format("头像路径 = %s\n", jo.opt("headPath")));
                sb.append("\n驾照专属字段\n");
                sb.append(String.format("国家 = %s\n", jo.opt("nation")));
                sb.append(String.format("初始领证 = %s\n", jo.opt("startTime")));
                sb.append(String.format("准驾车型 = %s\n", jo.opt("drivingType")));
                sb.append(String.format("有效期限 = %s\n", jo.opt("registerDate")));
                binding.textview.setText(sb.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        FileUtil.cleanDirectory(FileUtil.initPath());
    }


}
