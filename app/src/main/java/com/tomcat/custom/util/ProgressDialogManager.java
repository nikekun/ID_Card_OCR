package com.tomcat.custom.util;

import android.app.ProgressDialog;
import android.content.Context;

public class ProgressDialogManager {
    private Context context;
    private ProgressDialog progressDialog;

    public ProgressDialogManager(Context context) {
        this.context = context;
    }

    public void buildProgressDialog() {
        try {
            if (progressDialog == null) {
                progressDialog = new ProgressDialog(context);
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            }
            progressDialog.setMessage("识别中...");
            progressDialog.setCancelable(true);
            if (!progressDialog.isShowing())
                progressDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void cancelProgressDialog() {
        if (progressDialog != null) {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }
    }
}
